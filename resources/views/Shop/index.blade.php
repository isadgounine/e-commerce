@extends('home')
@section('content')
<div class="album py-5 bg-light">
    <div class="container">

      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
        {{-- {{ dump($produit) }}  --}}
        @foreach ($produit as $produits)
          <div class="col">
            <div class="card shadow-sm">
                <title>Placeholder</title>
                <img src="{{ asset('produits/'.$produits->photo_principale) }}" alt="" width="100%" height="100%" />
                <text x="50%" y="50%" fill="#eceeef" dy=".3em">{{ $produits->nom }}</text>
              <div class="card-body">
                <p class="card-text">{{ $produits->description }}</p>
                <div class="d-flex justify-content-between align-items-center">
                  <span class="price">{{ number_format($produits->prix_ht,2) }} €</span>
                  <a href="{{ route('voir_produit',['id'=>$produits->id]) }}" class="btn btn-sm btn-outline-secondary"><i class="fas fa-eye"></i></a>
              </div>
              </div>
            </div>
          </div>
        @endforeach

      </div>
    </div>
</div>
@endsection
