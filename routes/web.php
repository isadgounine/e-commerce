<?php

use App\Http\Controllers\Shop\MainController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

/* Appeler Classe 'MainController' de controller  */ 
    Route::get('/',  [MainController::class, 'index']);
    //or
    // Route::get('/',  "App\Http\Controllers\Shop\MainController@index");

    Route::get('/produit/{id}',[MainController::class, 'produit'])->name('voir_produit');

    Route::get('/categorie/{id}',[MainController::class, 'viewbycategorie'])->name('view_produit_by_category');