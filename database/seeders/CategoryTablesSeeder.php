<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoryTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new Category();
        $category->nom = 'Fimls';
        $category->is_online = 1;
        $category->save();

        $category = new Category();
        $category->nom = 'Séries TV';
        $category->is_online = 1;
        $category->save();

        $category = new Category();
        $category->nom = 'Musique';
        $category->is_online = 1;
        $category->save();

        $category = new Category();
        $category->nom = 'Jeux-védéos';
        $category->is_online = 1;
        $category->save();

        $category = new Category();
        $category->nom = 'Sport';
        $category->is_online = 1;
        $category->save();
    }
}
