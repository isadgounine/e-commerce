<?php

namespace Database\Seeders;

use App\Models\Produit;
use Illuminate\Database\Seeder;

class ProduitTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prduit = new Produit();
        $prduit->nom = "T-Shirt Rouge";
        $prduit->prix_ht = 10.0;
        $prduit->description = "mon 1 ere tshirt";
        $prduit->photo_principale = "rouge.jpg";
        $prduit->category_id = 1;
        $prduit->save();

        $prduit = new Produit();
        $prduit->nom = "T-Shirt jaune";
        $prduit->prix_ht = 20.0;
        $prduit->description = "mon 2 ere tshirt";
        $prduit->photo_principale = "rouge.jpg";
        $prduit->category_id = 1;
        $prduit->save();

        $prduit = new Produit();
        $prduit->nom = "T-Shirt blanc";
        $prduit->prix_ht = 30.0;
        $prduit->description = "mon 3 ere tshirt";
        $prduit->photo_principale = "rouge.jpg";
        $prduit->category_id = 6;
        $prduit->save();

        $prduit = new Produit();
        $prduit->nom = "T-Shirt vert";
        $prduit->prix_ht = 40.0;
        $prduit->description = "mon 4 ere tshirt";
        $prduit->photo_principale = "rouge.jpg";
        $prduit->category_id = 6;
        $prduit->save();

        $prduit = new Produit();
        $prduit->nom = "T-Shirt noir";
        $prduit->prix_ht = 50.0;
        $prduit->description = "mon 5 ere tshirt";
        $prduit->photo_principale = "rouge.jpg";
        $prduit->category_id = 6;
        $prduit->save(); 

        $prduit = new Produit();
        $prduit->nom = "T-Shirt noir";
        $prduit->prix_ht = 60.0;
        $prduit->description = "mon 6 ere tshirt";
        $prduit->photo_principale = "rouge.jpg";
        $prduit->category_id = 1;
        $prduit->save();

        $prduit = new Produit();
        $prduit->nom = "T-Shirt noir";
        $prduit->prix_ht = 70.0;
        $prduit->description = "mon 7 ere tshirt";
        $prduit->photo_principale = "rouge.jpg";
        $prduit->category_id = 2;
        $prduit->save();

        $prduit = new Produit();
        $prduit->nom = "T-Shirt noir";
        $prduit->prix_ht = 80.0;
        $prduit->description = "mon 8 ere tshirt";
        $prduit->photo_principale = "rouge.jpg";
        $prduit->category_id = 2;
        $prduit->save();

        $prduit = new Produit();
        $prduit->nom = "T-Shirt noir";
        $prduit->prix_ht = 90.0;
        $prduit->description = "mon 9 ere tshirt";
        $prduit->photo_principale = "rouge.jpg";
        $prduit->category_id = 3;
        $prduit->save();
    }
}
