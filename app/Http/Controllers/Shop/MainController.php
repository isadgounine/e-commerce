<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Produit;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        //select * produit
        $produit = Produit::all();
        /* selsct categorie */
        //$categorie = Category::where('is_online', 1)->get();
        
        //dd($produits);
        return view('Shop/index', compact('produit'));
    }

    public function produit(Request $request){
        //dump($_GET);
        //die();
        //print_r($_GET);
        //dd($request->id);
        //select * from produit where id=5
        //dd($request->id);
        $produit = Produit::find($request->id);
        /* selsct categorie */
        //$categorie = Category::where('is_online', 1)->get();

        return view('Shop/produit', compact('produit'));
    }

    public function viewbycategorie(Request $request){
        /* selsct categorie */
        //$categorie = Category::where('is_online', 1)->get();
        //dd($categorie);
        //dd($request);

        $produit = Produit::where('category_id',$request->id)->get();
        return view('shop.categorie', compact('produit'));
    }
}
