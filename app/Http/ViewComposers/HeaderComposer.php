<?php

namespace App\Http\ViewComposers;

use App\Models\Category;
use Illuminate\View\View;

class headerComposer 
{

    public function compose(View $view)
    {
        $view->with('categorie',Category::where('is_online', 1)->get());
    }

}